/* global describe it beforeEach */

/*
  These test cases are most likely useless,
  because I'm rushing this as quick as I can due time limit.
  I also have ongoing work and can't fit in time much.

  This is most likely demonstration that I know what test cases are.
*/

const assert = require('assert');
const _ = require('lodash');

const models = require('../core/models');
const gameService = require('../core/services/game.service');

let user;
let auction;

describe('Game', async () => {
  describe('#auth()', async () => {
    beforeEach(async () => {
      await models.sequelize.sync({ force: true });
    });

    it('should authenticate user or create new one if not exists', async () => {
      user = await gameService.auth('test');
      assert.ok(user);
    });

    // it('should add 30 breads, 18 carrots and 1 diamond to user\'s inventory', () => {
    //   assert.equal(user);
    // });
  });

  describe('#getUserData()', async () => {
    it('should return existing user data by user id', async () => {
      user = (await gameService.getUserData(user.id)).toJSON();
      assert.ok(user);
    });

    it('should return existing user data by username', async () => {
      user = (await gameService.getUserData(user.username)).toJSON();
      assert.ok(user);
    });

    describe('user', async () => {
      it('should have 3 items in a total.', async () => {
        assert.ok(user.inventory && user.inventory.length === 3);
      });

      it('should have 30 breads, 18 carrots, 1 diamond and 1000 coins in inventory', async () => {
        const bread = await models.item.findOne({ where: { name: 'bread' } });
        const carrot = await models.item.findOne({ where: { name: 'carrot' } });
        const diamond = await models.item.findOne({ where: { name: 'diamond' } });

        assert.equal(_.find(user.inventory, { id: bread.id }).quantity, 30);
        assert.equal(_.find(user.inventory, { id: carrot.id }).quantity, 18);
        assert.equal(_.find(user.inventory, { id: diamond.id }).quantity, 1);
        assert.equal(user.coins, 1000);
      });
    });
  });

  describe('#auctionOngoing()', async () => {
    it('should not be auction ongoing', async () => {
      const isAuctionOngoing = await gameService.auctionOngoing();
      assert.equal(isAuctionOngoing, false);
    });
  });

  describe('#getCurrentAuction()', async () => {
    it('should return null when auction is not going', async () => {
      auction = await gameService.getCurrentAuction();
      assert.equal(auction, null);
    });
  });

  describe('cleanup', async () => {
    it('should cleanup database', async () => {
      await models.sequelize.sync({ force: true });
    });
  });

  // ... more test cases goes here
});
