module.exports = {
  server: {
    port: process.env.NODE_ENV === 'production' ? 80 : 8080,
  },
  db: {
    host: '127.0.0.1',
    port: 3306,
    user: 'root',
    password: 'mysecurepassword',
    database: 'crossover_auction_system_game',
    supportBigNumbers: true,
    bigNumberStrings: true,
    multipleStatements: true,
  },
};
