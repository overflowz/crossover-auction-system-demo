const socketHandler = require('./socket.handler');

module.exports = {
  Socket: socketHandler,
};
