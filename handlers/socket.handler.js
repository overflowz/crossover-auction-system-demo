// const models = require('../core/models');
const services = require('../core/services');
const clients = require('../core/utils/clients');

class Client {
  constructor(io, socket) {
    this.io = io;
    this.socket = socket;
    this.socket.auth = false;

    this.authDisconnectTimer = setTimeout(() => {
      if (this.socket && !this.socket.auth) this.socket.disconnect(true);
    }, 5000);

    this.setupEvents();
  }

  setupEvents() {
    this.socket.on('disconnect', this.onDisconnect.bind(this));
    this.socket.on('auth', this.onAuth.bind(this));
    this.socket.on('me', this.onMe.bind(this));
    this.socket.on('bid', this.onBid.bind(this));
    this.socket.on('auction', this.onAuction.bind(this));
    this.socket.on('current-auction', this.onCurrentAuction.bind(this));
  }

  async _broadcastAuctionData() {
    const currentAuction = await services.Game.getCurrentAuction();
    return this.io.sockets.emit('current-auction', currentAuction);
  }

  // we're not removing socket from nsps to
  // not get broadcast data if not authenticated,
  // for the sake of demo.
  async onAuth(username) {
    if (!this.socket.auth && username) {
      const user = await services.Game.auth(username);

      const connectedClients = this.io.sockets.clients();

      // disconnect other connected user with the same username.
      Object.keys(connectedClients.connected).forEach((client) => {
        const connectedClient = connectedClients.connected[client];
        if (
          connectedClient && connectedClient.user &&
          connectedClient.user.username === user.username &&
          connectedClient.id !== this.socket.id
        ) {
          connectedClient.disconnect(true);
        }
      });

      this.socket.auth = true;
      this.socket.user = user.toJSON();

      clients[username] = this.socket;

      clearTimeout(this.socket.authDisconnectTimer);
      this.socket.emit('auth', 'success');
    }
  }

  async onMe() {
    if (!this.socket.auth || !this.socket.user) {
      return this.socket.disconnect(true);
    }

    const user = await services.Game.getUserData(this.socket.user.username);

    return this.socket.emit('me', user.toJSON());
  }

  async onAuction(auctionData) {
    if (!this.socket.auth || !this.socket.user) {
      return this.socket.disconnect(true);
    }

    try {
      await services.Game.auction(
        this.socket.user.username,
        auctionData,
        async (seller, winner) => {
          try {
            if (clients[this.socket.user.username]) {
              clients[this.socket.user.username].emit('me', seller);
            }

            if (winner.id !== seller.id && clients[winner.username]) {
              clients[winner.username].emit('me', winner);
            }

            await this._broadcastAuctionData();
          } catch (err) {
            console.error(err);
          }
        },
      );

      const user = await services.Game.getUserData(this.socket.user.username);

      if (clients[this.socket.user.username]) {
        clients[this.socket.user.username].emit('me', user);
      }

      return this._broadcastAuctionData();
    } catch (err) {
      return this.socket.emit('auction', { success: false, error: err.message });
    }
  }

  async onCurrentAuction() {
    if (!this.socket.auth || !this.socket.user) {
      return this.socket.disconnect(true);
    }

    const currentAuction = await services.Game.getCurrentAuction();
    return this.socket.emit('current-auction', currentAuction);
  }

  async onBid(bidData) {
    if (!this.socket.auth || !this.socket.user) {
      return this.socket.disconnect(true);
    }

    try {
      await services.Game.makeBid(this.socket.user.username, bidData);
      await this._broadcastAuctionData();
    } catch (err) {
      return this.socket.emit('auction', { success: false, error: err.message });
    }

    return this._broadcastAuctionData();
  }

  onDisconnect() {
    clearTimeout(this.authDisconnectTimer);
  }
}

module.exports = (io, socket) => {
  return new Client(io, socket);
};
