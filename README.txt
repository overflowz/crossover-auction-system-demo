This application implements everything mentioned in the assignment document.

Also, was rushing as fast as I could, due I also have work ongoing and wasn't able to continue much.

As for frontend side, I was learning AngularJS while doing this demo.

Requirements: NodeJS 8.x
How to run: npm install && npm start
tests: npm test (this is basic tests, due time limit, there's also some notes in test/test.js)

what to improve (feedback from crossover):

Front End:
- Scripts in 
- Inline style in html template
- Empty directives and service files
- jQuery usage instead of Angular tools
- Magic numbers (like 1000, 10)
- Code duplication in portal.controller (62-65 lines and 71-74)
- Business logic on controller side instead of service usage (reuseful should be better)
- Possible memory leak (scope on destroy was not used - socket still open)
- Unit tests not provided
- Comments coverage should be better
- Exception handling should be better

Backend:
- System design and modularity should be better (ex. game.service - auth and auction functionality in one file)
- Comments coverage should be better