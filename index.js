const express = require('express');
const io = require('socket.io');
const bodyParser = require('body-parser');
const path = require('path');

const config = require('./config');
const handlers = require('./handlers');

const app = express();

if (process.env.NODE_ENV !== 'production') {
  // allow CORS for testing.
  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT,GET,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, x-access-token');

    if (req.method === 'OPTIONS') {
      return res.status(200).end();
    }

    return next();
  });
}

app.set('env', 'production');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/assets/', express.static(path.join(__dirname, 'bower_components')));
app.use(express.static(path.join(__dirname, 'public')));

const listener = app.listen(config.server.port, () => {
  console.log(`server started on ${listener.address().port}`);
});

const sio = io(listener);

sio.on('connection', (socket) => {
  handlers.Socket(sio, socket);
});
