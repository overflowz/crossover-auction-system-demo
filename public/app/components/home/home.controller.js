/* global app */

app.controller('homeController', ['$scope', '$location', 'socket', function ($scope, $location, socket) {
  $scope.user = '';

  $scope.login = function () {
    if (socket.socket() && socket.socket().connected) {
      socket.socket().disconnect();
    }

    socket.connect();

    socket.socket().on('auth', function (resp) {
      if (resp) {
        $scope.$apply(function () {
          $location.path('portal');
        });
      }
    });

    socket.socket().emit('auth', $scope.user);
  };
}]);
