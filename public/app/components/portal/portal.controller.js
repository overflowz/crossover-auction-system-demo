/* global app moment $ */

app.controller('portalController', ['$scope', '$location', '$interval', 'socket', function ($scope, $location, $interval, socket) {
  $scope.user = {};
  $scope.auctionData = {};
  $scope.bidAmount = 0;
  $scope.errorMessage = '';

  $scope.startAuctionData = {
    itemId: null,
    quantity: null,
    minimumBid: null,
  };

  if (!socket.socket() || !socket.socket().connected) {
    return $location.path('/');
  }

  socket.socket().on('auction', function (resp) {
    if (resp && !resp.success) {
      $scope.$apply(function () {
        $scope.errorMessage = resp.error;
        $('#errorModal').modal('show');
      });
    }
  });

  socket.socket().on('bid', function (resp) {
    if (resp && !resp.success) {
      $scope.$apply(function () {
        $scope.errorMessage = resp.error;
        $('#errorModal').modal('show');
      });
    }
  });

  $scope.showAuctionDialog = function (item) {
    $scope.startAuctionData.itemId = item.id;
    $('#startAuctionModal').modal('show');
  };

  $scope.startAuction = function () {
    socket.socket().emit('auction', $scope.startAuctionData);
  };

  $scope.placeBid = function () {
    socket.socket().emit('bid', {
      amount: $scope.bidAmount,
    });
  };

  socket.socket().on('me', function (user) {
    $scope.user = user;
    $scope.$apply();
  });

  socket.socket().on('current-auction', function (auctionData) {
    $scope.auctionData = auctionData ? Object.assign({}, auctionData) : null;

    if (auctionData) {
      $scope.auctionData.timeLeft =
        Math.floor(parseInt(moment(auctionData.timeLeft)
          .add(90, 'seconds')
          .add(-Date.now(), 'milliseconds')
          .valueOf() / 1000, 10));
    }

    var tick = function () {
      if ($scope.auctionData && $scope.auctionData.timeLeft) {
        $scope.auctionData.timeLeft =
          Math.floor(parseInt(moment(auctionData.timeLeft)
            .add(90, 'seconds')
            .add(-Date.now(), 'milliseconds')
            .valueOf() / 1000, 10));

        if ($scope.auctionData.timeLeft <= 0) {
          if ($scope.timerHandle) {
            $interval.cancel($scope.timerHandle);
            $scope.timerHandle = null;
          }
        } else {
          // request current auction data again, just in case.
          // socket.socket().emit('current-auction');
        }
      }
    };

    if (auctionData && $scope.auctionData.timeLeft > 0) {
      if ($scope.timerHandle) {
        $interval.cancel($scope.timerHandle);
        $scope.timerHandle = null;
      }

      $scope.timerHandle = $interval(tick, 1000);
    }

    $scope.$apply();
  });

  socket.socket().emit('me');
  socket.socket().emit('current-auction');

  $scope.logout = function () {
    socket.socket().disconnect();
  };
}]);
