/* global angular */

const app = angular.module('myApp', ['ngRoute']);

app.config(function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: '/app/components/home/home.view.html',
      controller: 'homeController',
    })
    .when('/portal', {
      templateUrl: '/app/components/portal/portal.view.html',
      controller: 'portalController',
    });
});
