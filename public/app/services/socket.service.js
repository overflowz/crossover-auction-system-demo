/* global app io */

app.factory('socket', ['$location', '$timeout', function ($location, $timeout) {
  var _socket = null;

  return {
    connect: function () {
      _socket = io.connect();

      _socket.on('disconnect', function () {
        $timeout(function () {
          $location.path('/');
        });
      });
    },
    socket: function () {
      return _socket;
    },
  };
}]);
