/* global app */

app.directive('playerStatsWidget', [function () {
  return {
    restrict: 'E',
    scope: false,
    templateUrl: '/app/widgets/player-stats/player-stats.view.html',

    link: function () {
      console.log('playerStatsWidget link');
    },
  };
}]);
