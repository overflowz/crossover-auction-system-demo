/* global app */

app.directive('currentAuctionWidget', [function () {
  return {
    restrict: 'E',
    scope: false,
    templateUrl: '/app/widgets/current-auction/current-auction.view.html',

    link: function () {
      console.log('currentAuctionWidget link');
    },
  };
}]);
