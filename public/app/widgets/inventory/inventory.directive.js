/* global app */

app.directive('inventoryWidget', [function () {
  return {
    restrict: 'E',
    scope: false,
    templateUrl: '/app/widgets/inventory/inventory.view.html',

    link: function () {
      console.log('inventoryWidget link');
    },
  };
}]);
