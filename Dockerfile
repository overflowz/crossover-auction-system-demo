FROM node:8.9.3

WORKDIR /usr/src/app

# need this to bypass policy
RUN echo exit 0 > /usr/sbin/policy-rc.d

# install mysql server
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install mysql-server

# mysql setup
RUN service mysql start && sleep 5 && mysql --protocol=tcp -u root -e "CREATE DATABASE IF NOT EXISTS crossover_auction_system_game"
RUN service mysql start && sleep 5 && mysqladmin -u root password mysecurepassword

# install bower
RUN npm install -g bower

# copy package.json and run npm install
COPY package*.json ./
RUN npm install

# copy all other required files
COPY . .

# allow bower to be ran as a root.
RUN echo '{ "allow_root": true }' > /root/.bowerrc

# install bower components
RUN bower install

# expose 8080 and 3306 ports
EXPOSE 8080 3306

# run!
RUN chmod +x /usr/src/app/entrypoint.sh
ENTRYPOINT [ "/usr/src/app/entrypoint.sh" ]
# CMD ["npm", "start"]
