module.exports = {
  "extends": "airbnb-base",
  "rules": {
    "object-shorthand": "off",
    "prefer-arrow-callback": "off",
    "func-names": "off",
    "no-var": "off",
    "prefer-rest-params": "off",
    "arrow-body-style": "off",
    "no-underscore-dangle": "off",
    "class-methods-use-this": "off",
    "vars-on-top": "off",
    "no-param-reassign": "off"
  }
};