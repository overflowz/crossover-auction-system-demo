/* eslint func-names: 0 */

const Sequelize = require('sequelize');
const config = require('../../config');

const sequelize = new Sequelize(config.db.database, config.db.user, config.db.password, {
  host: config.db.host,
  dialect: 'mysql',
  logging: process.env.NODE_ENV === 'development',
});

// define sequelize class, instance & models.
const models = {
  Sequelize,
  sequelize,
  user: sequelize.import('./user.model.js'),
  inventory: sequelize.import('./inventory.model.js'),
  auction: sequelize.import('./auction.model.js'),
  item: sequelize.import('./item.model.js'),
};

// call associations if present.
Object.keys(models).forEach((modelName) => {
  if (Object.prototype.hasOwnProperty.call(models[modelName], 'associate')) {
    models[modelName].associate(models);
  }
});

sequelize.sync({ force: true }).then(() => {
  // seed initial data (for the sake of demo).
  return models.item.bulkCreate([{
    name: 'bread',
  }, {
    name: 'carrot',
  }, {
    name: 'diamond',
  }]);
});

module.exports = models;
