/* eslint func-names: 0 */

const schema = function (sequelize, Types) {
  const model = sequelize.define('inventory', {
    id: {
      type: Types.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    quantity: {
      type: Types.INTEGER,
      allowNull: false,
    },
  });

  model.associate = (models) => {
    model.belongsTo(models.user);
    model.belongsTo(models.item, { as: 'item' });
  };

  return model;
};

module.exports = schema;
