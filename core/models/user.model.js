/* eslint func-names: 0 */

const schema = function (sequelize, Types) {
  const model = sequelize.define('user', {
    id: {
      type: Types.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    username: {
      type: Types.STRING,
      allowNull: false,
      unique: true,
    },
    coins: {
      type: Types.INTEGER,
      allowNull: false,
      // A new player starts with a balance of 1000 coins
      defaultValue: 1000,
    },
  });

  model.associate = (models) => {
    model.hasMany(models.inventory, { as: 'inventory' });
  };

  return model;
};

module.exports = schema;
