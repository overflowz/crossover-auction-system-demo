/* eslint func-names: 0 */

const schema = function (sequelize, Types) {
  const model = sequelize.define('auction', {
    id: {
      type: Types.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    quantity: {
      type: Types.INTEGER,
      allowNull: false,
    },
    timeLeft: {
      type: Types.DATE,
      allowNull: false,
      defaultValue: sequelize.NOW,
    },
    // startingBid: {
    //   type: Types.INTEGER,
    //   allowNull: false,
    // },
    winningBid: {
      type: Types.INTEGER,
      allowNull: false,
    },
    isClosed: {
      type: Types.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
  });

  model.associate = (models) => {
    model.belongsTo(models.item, { as: 'item' });
    model.belongsTo(models.user, { as: 'user' });
    model.belongsTo(models.user, { as: 'winningUser' });
  };

  return model;
};

module.exports = schema;
