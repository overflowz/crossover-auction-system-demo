/* eslint func-names: 0 */

const schema = function (sequelize, Types) {
  const model = sequelize.define('item', {
    id: {
      type: Types.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: Types.STRING,
      allowNull: false,
      unique: true,
    },
  });

  model.associate = (models) => {
    model.hasMany(models.inventory, { as: 'inventory' });
  };

  return model;
};

module.exports = schema;
