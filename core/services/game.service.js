// putting all together for the sake of demo.
const models = require('../models');
const moment = require('moment');
const _ = require('lodash');

async function getUserData(userIdOrName) {
  const where = {};

  if (typeof userIdOrName === 'string') {
    where.username = userIdOrName;
  } else {
    where.id = userIdOrName;
  }

  const user = await models.user.findOne({
    where,
    include: [{
      model: models.inventory,
      as: 'inventory',
      include: [{
        model: models.item,
        as: 'item',
      }],
    }],
  });

  return user;
}

async function auth(username) {
  let user = await models.user.findOne({
    where: {
      username: username,
    },
  });

  if (!user) {
    user = await models.user.create({ username });

    // for the sake of demo.
    const bread = await models.item.find({ where: { name: 'bread' } });
    const carrot = await models.item.find({ where: { name: 'carrot' } });
    const diamond = await models.item.find({ where: { name: 'diamond' } });

    // create inventory for the user.
    await models.inventory.bulkCreate([{
      itemId: bread.id, quantity: 30, userId: user.id,
    }, {
      itemId: carrot.id, quantity: 18, userId: user.id,
    }, {
      itemId: diamond.id, quantity: 1, userId: user.id,
    }]);
  }

  return user;
}

async function getCurrentAuction() {
  const currentAuction = await models.auction.findOne({
    where: {
      isClosed: false,
    },
    include: [{
      model: models.item, as: 'item',
    }, {
      model: models.user, as: 'user',
    }],
  });

  return currentAuction;
}

async function auctionOngoing() {
  return !!(await getCurrentAuction());
}

async function finishAuction(auctionModel) {
  auctionModel.isClosed = true;

  const transaction = await models.sequelize.transaction();

  if (auctionModel.userId !== auctionModel.winningUserId) {
    const seller = await models.user.findById(auctionModel.userId);
    const winner = await models.user.findById(auctionModel.winningUserId);

    winner.coins -= auctionModel.winningBid;
    seller.coins += auctionModel.winningBid;
    // seller.coins += auctionModel.startingBid;

    await winner.save({ transaction });
    await seller.save({ transaction });
  }

  const itemInInventory = await models.inventory.find({
    where: {
      userId: auctionModel.winningUserId,
      itemId: auctionModel.itemId,
    },
    transaction,
  });

  if (itemInInventory) {
    itemInInventory.quantity += auctionModel.quantity;
    await itemInInventory.save({ transaction });
  } else {
    await models.inventory.create({
      itemId: auctionModel.itemId,
      userId: auctionModel.winningUserId,
      quantity: auctionModel.quantity,
    }, { transaction });
  }

  await auctionModel.save({ transaction });
  await transaction.commit();
}

async function auction(username, auctionData, onAuctionFinishedCallback) {
  const user = await getUserData(username);

  // not checking variable types for the sake of simplicity.
  // could've used joi library for that.
  if (!auctionData) {
    throw new Error('invalid request');
  } else if (await auctionOngoing()) {
    throw new Error('auction already in progress');
  } else if (!auctionData.itemId || !_.find(user.inventory, { itemId: auctionData.itemId })) {
    throw new Error('invalid item');
  } else if (!auctionData.quantity ||
    !_.filter(
      user.inventory,
      (item) => {
        return item.itemId === auctionData.itemId && item.quantity >= auctionData.quantity;
      },
    ).length) {
    throw new Error('invalid quantity');
  } else if (!auctionData.minimumBid || auctionData.minimumBid > user.coins) {
    throw new Error('invalid bid');
  }

  const transaction = await models.sequelize.transaction();
  // user.coins -= auctionData.minimumBid;
  const timeLeft = Date.now();

  const runningAuction = await models.auction.create({
    quantity: auctionData.quantity,
    timeLeft,
    // startingBid: auctionData.minimumBid,
    winningBid: auctionData.minimumBid,
    isClosed: false,
    itemId: auctionData.itemId,
    userId: user.id,
    winningUserId: user.id,
  }, { transaction });

  user.inventory.filter(i => i.itemId === auctionData.itemId).forEach((item) => {
    item.quantity -= auctionData.quantity;
    if (!item.quantity) {
      item.destroy({ transaction });
    }

    item.save({ transaction });
  });

  await user.save({ transaction });
  await transaction.commit();

  // register a basic timer to end auction.
  setTimeout(async function tick(auctionId) {
    try {
      const currentAuction = await getCurrentAuction();
      if (!currentAuction || currentAuction.id !== auctionId) return;

      const auctionTimeLeft =
        Math.floor(parseInt(moment(currentAuction.timeLeft)
          .add(90, 'seconds')
          .add(-Date.now(), 'milliseconds')
          .valueOf() / 1000, 10));

      if (auctionTimeLeft <= 0) {
        await finishAuction(currentAuction);
        if (onAuctionFinishedCallback) {
          const seller = await getUserData(username);
          const winner = await getUserData(currentAuction.winningUserId);

          onAuctionFinishedCallback(seller, winner);
        }
      } else {
        setTimeout(tick.bind(this, auctionId), 1000);
      }

      // return null;
    } catch (err) {
      console.error(err);
    }
  }.bind(this, runningAuction.id), 1000);
}

async function makeBid(username, bidData) {
  const user = await getUserData(username);
  const currentAuction = await getCurrentAuction();

  if (!currentAuction) {
    throw new Error('no active auctions found');
  }

  if (!bidData) {
    throw new Error('invalid request');
  } else if (!bidData.amount || bidData.amount > user.coins ||
    bidData.amount <= currentAuction.winningBid) {
    throw new Error('invalid amount');
  }

  currentAuction.winningBid = bidData.amount;
  currentAuction.winningUserId = user.id;

  const auctionTimeLeft =
    Math.floor(parseInt(moment(currentAuction.timeLeft)
      .add(90, 'seconds')
      .add(-Date.now(), 'milliseconds')
      .valueOf() / 1000, 10));

  // if auction time is less than 10 seconds, increase to 10.
  if (auctionTimeLeft < 10) {
    currentAuction.timeLeft =
      moment(currentAuction.timeLeft)
        .add(10, 'seconds')
        .add(-auctionTimeLeft, 'seconds')
        .toDate();
  }

  await currentAuction.save();
}

module.exports = {
  getUserData,
  auth,
  getCurrentAuction,
  auctionOngoing,
  auction,
  makeBid,
};
